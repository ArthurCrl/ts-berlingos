import Ingredient from './Ingredient'
import Tool from './Tool'

export enum EDifficulty {
  Easy = 0,
  Medium = 1,
  Hard = 2
}
export default class Recipe {
  public ingredients: Ingredient[]
  public tools?: Tool[]
  public difficulty: EDifficulty

  public name: string

  public steps: string[]

  public preparationDurationInMinutes: number
  public cookingDurationInMinutes?: number
}
