
export type TypeWithAdditionalMandatoryProps<T, P extends keyof T> = T & {
  [K in NonNullable<keyof Pick<T, P>>]: NonNullable<Pick<T, P>[K]>
}

export type JustProps<T> = Pick<T, NonNullable<({
  [Property in keyof T]: T[Property] extends Function ? never : Property })[keyof T]>>

export type JustMethods<T> = Pick<T, ({
  [Method in keyof T]: T[Method] extends Function ? Method : never })[keyof T]>

export type PartialProps<T> = JustMethods<T> & Partial<JustProps<T>>
