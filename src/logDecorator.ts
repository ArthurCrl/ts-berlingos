export type LogLevel = 'debug' | 'perf' | 'info' | 'warn' | 'error' | string
export type Logger = { [K in LogLevel]: (msg: string) => void }
export enum EMethodStatus {
  START = 'Start',
  END = 'End',
}

function logStart({ methodName, args, withArgs, level, prefix, logger }: {
  methodName: string,
  level: LogLevel,
  prefix?: string,
  args?: IArguments,
  withArgs?: boolean,
  logger: Logger,
}) {
  const msgPrefix = !!prefix ? `[${prefix}]` : ''
  const msgPostfix = withArgs ? ` with args ${JSON.stringify(args)}` : ''
  logger[level](`${msgPrefix}[${methodName}][${EMethodStatus.START}]${msgPostfix}`)
}

export function logEndAndReturn<T>({ methodName, result, withResult, level, prefix, logger, time }: {
  methodName: string,
  level: LogLevel,
  prefix?: string,
  result: T,
  withResult: boolean,
  logger: Logger,
  time: [number, number] | undefined
}): T {
  const msgPrefix = !!prefix ? `[${prefix}]` : ''
  const msgPostfix = withResult ? ` with result ${JSON.stringify(result)}` : ''
  logger[level](`${msgPrefix}[${methodName}][${EMethodStatus.END}]${msgPostfix}`)
  if (time) {
    const endTime = process.hrtime(time)
    const processTime = {
      seconds: endTime[0],
      ms: Math.round(endTime[1] / Math.pow(10, 6)),
    }
    logger[level](`${msgPrefix}[${methodName}][${EMethodStatus.END}] processing time: ${processTime.seconds}.${processTime.ms}s`)

  }
  return result
}

export function LogStartEnd(logger: Logger, options?: {
  withArgs?: boolean,
  withResult?: boolean,
  withPrefix?: boolean,
  withTime?: boolean,
  level?: LogLevel
}) {
  return (
    target: any,
    propertyKey: string,
    propertyDescriptor: PropertyDescriptor
  ) => {
    const originalFunction = propertyDescriptor.value
    const withArgs = options?.withArgs ?? true
    const withResult = options?.withResult ?? true
    const withPrefix = options?.withPrefix ?? true
    const withTime = options?.withTime ?? true
    const level = options?.level ?? 'info'
    propertyDescriptor.value = function () {
      const startTime = withTime ? process.hrtime() : undefined
      logStart({
        methodName: propertyKey,
        args: arguments,
        prefix: withPrefix ? this.constructor.name : '',
        level: level ?? 'info',
        withArgs,
        logger
      })
      const result = originalFunction.apply(this, arguments)
      if (result.then !== undefined) {
        return result.then((r: any) => logEndWithParamsAndReturn({
          propertyKey,
          r,
          options: {
            withArgs, withResult, withPrefix, withTime, level
          },
          startTime,
          logger,
          constructorName: this.constructor.name
        }))
      } else {
        return logEndWithParamsAndReturn({
          propertyKey,
          r: result,
          options: {
            withArgs, withResult, withPrefix, withTime, level
          },
          startTime,
          logger,
          constructorName: this.constructor.name
        })
      }
    }
    return propertyDescriptor
  }
}
function logEndWithParamsAndReturn({ propertyKey, r, options, startTime, logger, constructorName }: {
  propertyKey: string,
  r: any,
  options: {
    withArgs?: boolean
    withResult?: boolean
    withPrefix?: boolean
    withTime?: boolean
    level?: LogLevel
  } | undefined,
  startTime: [number, number] | undefined,
  constructorName: string,
  logger: Logger
}) {
  return logEndAndReturn({
    methodName: propertyKey,
    result: r,
    withResult: !!options?.withResult,
    prefix: !!options?.withPrefix ? constructorName : undefined,
    level: options?.level ?? 'info',
    time: startTime,
    logger,
  })
}
