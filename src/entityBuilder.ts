
import * as _ from 'lodash'
import { PartialProps, TypeWithAdditionalMandatoryProps } from 'src/types/utilityTypes'
type BuilderWithTypeMethods<T, IT> = EntityBuilder<T, IT> & {
  // tslint:disable-next-line: ban-types
  [K in NonNullable<keyof T> as T[K] extends Function ? never : `with${Capitalize<string & K>}`]:
  (value: NonNullable<T[K]>) => BuilderWithTypeMethods<TypeWithAdditionalMandatoryProps<T, K>, IT>
}
export default class EntityBuilder<BuiltEntityType, InitialType> {

  public static getA<StaticBuiltEntityType>(TCreator: new () => StaticBuiltEntityType) {
    const proxy: EntityBuilder<StaticBuiltEntityType, StaticBuiltEntityType> = new Proxy(new EntityBuilder(TCreator), {
      get(target, propKey: keyof EntityBuilder<StaticBuiltEntityType, StaticBuiltEntityType>) {
        if (propKey.startsWith('with')) {
          const prop = _.camelCase(propKey.replace('with', '')) as keyof StaticBuiltEntityType
          return (arg: any) => {
            target.builtEntity[prop] = arg
            return proxy
          }
        } else {
          return target[propKey]
        }
      },
    })
    return proxy as unknown as BuilderWithTypeMethods<PartialProps<StaticBuiltEntityType>, StaticBuiltEntityType>
  }

  public static getAn<StaticBuiltEntityType>(TCreator: new () => StaticBuiltEntityType) {
    return EntityBuilder.getA(TCreator)
  }

  protected builtEntity: Partial<BuiltEntityType>
  private creator: new () => BuiltEntityType

  constructor(TCreator: new () => BuiltEntityType) {
    this.creator = TCreator
    this.builtEntity = new this.creator()
  }

  public build() {
    const returnedObj = this.builtEntity as BuiltEntityType
    this.builtEntity = new this.creator()
    return returnedObj;
  }

  public byCopyFrom(e: BuiltEntityType) {
    this.builtEntity = _.merge(this.builtEntity, _.cloneDeep(e))
    return this as unknown as BuilderWithTypeMethods<InitialType, InitialType>
  }

  public fromInputs< K extends keyof BuiltEntityType>(e?: {
    [k in K]: BuiltEntityType[k]
  }) {
    this.builtEntity = _.merge(this.builtEntity, e);
    return this as unknown as BuilderWithTypeMethods<TypeWithAdditionalMandatoryProps<BuiltEntityType, K>
      , InitialType>
  }

}
